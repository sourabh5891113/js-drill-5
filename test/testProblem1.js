// Iterate over each object in the array and in the email breakdown the email and return the output as below:
/*
     Note: use only email property to get the below details
  
     [
        {
        firstName: 'John', // the first character should be in Uppercase
        lastName: 'Doe', // the first character should be in Uppercase
        emailDomain: 'example.com'
        },
        {
        firstName: 'Jane',
        lastName: 'Smith',
        emailDomain: 'gmail.com'
        }
        .. so on
     ]
  */

let dataSet = require("../data.js");
let getNameAndEmail = require("../problems/problem1.js");
console.log(getNameAndEmail(dataSet));
