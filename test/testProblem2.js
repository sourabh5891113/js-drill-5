// Check the each person projects list and group the data based on the project status and return the data as below
/*
  
    {
      'Completed': ["Mobile App Redesign","Product Launch Campaign","Logo Redesign","Project A".. so on],
      'Ongoing': ["Website User Testing","Brand Awareness Campaign","Website Mockup" .. so on]
      .. so on
    }
  
  */

let dataSet = require("../data.js");
let getProjectsStatus = require("../problems/problem2.js");
console.log(getProjectsStatus(dataSet));
