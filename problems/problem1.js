function capitalizeFirstChar(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

function getNameAndEmail(dataSet) {
  if (Array.isArray(dataSet)) {
    let result = dataSet.map((person) => {
      let email = person.email;
      let nameAndMail = email.split("@");
      let name = nameAndMail[0];
      let emailDomain = nameAndMail[1];
      let fullName = name.split(".");
      let firstName = capitalizeFirstChar(fullName[0]);
      let lastName = capitalizeFirstChar(fullName[1]);
      return {
        firstName: firstName,
        lastName: lastName,
        emailDomain: emailDomain,
      };
    });

    return result;
  } else {
    console.log("First argument should be an array");
    return [];
  }
}

module.exports = getNameAndEmail; 
