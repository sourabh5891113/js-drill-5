function getUniqueLanguages(dataSet) {
  if (Array.isArray(dataSet)) {
    let uniqueLanguages = dataSet.reduce((acc, person) => {
      person.languages.forEach((language) => {
        if (!acc.some((val) => val === language)) {
          acc.push(language);
        }
      });
      return acc;
    }, []);
    return uniqueLanguages;
  } else {
    console.log("First argument should be an array");
    return [];
  }
}

module.exports = getUniqueLanguages;
