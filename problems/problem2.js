function getProjectsStatus(dataSet) {
  if (Array.isArray(dataSet)) {
    let projectsStatus = dataSet.reduce((acc, person) => {
      let projects = person.projects;
      for (let key of projects) {
        if (key.status === "Completed") {
          if (acc.Completed) {
            acc.Completed = [...acc.Completed, key.name];
          } else {
            acc.Completed = [key.name];
          }
        } else if (key.status === "Ongoing") {
          if (acc.Ongoing) {
            acc.Ongoing = [...acc.Ongoing, key.name];
          } else {
            acc.Ongoing = [key.name];
          }
        } else {
          if (acc.In_Progress) {
            acc.In_Progress = [...acc.In_Progress, key.name];
          } else {
            acc.In_Progress = [key.name];
          }
        }
      }
      return acc;
    }, {});
    return projectsStatus;
  } else {
    console.log("First argument should be an array");
    return [];
  }
}

module.exports = getProjectsStatus; 
